package com.rizqi.yummlyrun;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by rizqi on 03/04/2017.
 */

public class YummlyAppDroid extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        MultiDex.install(this);
    }

    public static Context getContext(){
        return mContext;
    }
}