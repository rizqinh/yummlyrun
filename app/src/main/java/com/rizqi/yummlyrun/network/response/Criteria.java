package com.rizqi.yummlyrun.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rizqi on 16/03/2017.
 */

public class Criteria {

    @SerializedName("q")
    @Expose
    public String q;
    @SerializedName("allowedIngredient")
    @Expose
    public String allowedIngredient;
    @SerializedName("excludedIngredient")
    @Expose
    public String excludedIngredient;

}
