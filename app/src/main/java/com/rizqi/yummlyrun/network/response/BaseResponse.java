package com.rizqi.yummlyrun.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rizqi on 15/03/2017.
 */

public class BaseResponse implements Parcelable{

    public BaseResponse(){

    }

    @SerializedName("criteria")
    @Expose
    public Criteria criteria;
    @SerializedName("facetCounts")
    @Expose
    public FacetCounts facetCounts;
    @SerializedName("totalMatchCount")
    @Expose
    public int totalMatchCount;
    @SerializedName("attribution")
    @Expose
    public Attribution attribution;

    protected BaseResponse(Parcel in) {
        totalMatchCount = in.readInt();
    }

    public static final Creator<BaseResponse> CREATOR = new Creator<BaseResponse>() {
        @Override
        public BaseResponse createFromParcel(Parcel in) {
            return new BaseResponse(in);
        }

        @Override
        public BaseResponse[] newArray(int size) {
            return new BaseResponse[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseResponse that = (BaseResponse) o;

        if (totalMatchCount != that.totalMatchCount) return false;
        if (!criteria.equals(that.criteria)) return false;
        if (!facetCounts.equals(that.facetCounts)) return false;
        return attribution.equals(that.attribution);

    }

    @Override
    public int hashCode() {
        int result = criteria.hashCode();
        result = 31 * result + facetCounts.hashCode();
        result = 31 * result + totalMatchCount;
        result = 31 * result + attribution.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "criteria=" + criteria +
                ", facetCounts=" + facetCounts +
                ", totalMatchCount=" + totalMatchCount +
                ", attribution=" + attribution +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(totalMatchCount);
    }
}
