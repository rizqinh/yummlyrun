package com.rizqi.yummlyrun.network

/**
 * Created by rizqi on 15/03/2017.
 */

class ApiConstant {
    companion object {
        const val BASE_URL: String = "https://api.yummly.com"


        //Parameter
        const val BASE_LIST_FOOD: String = "/v1/api/recipes"
        const val SEARCH_LIST_FOOD: String = "/v1/api/recipes"
    }
}
