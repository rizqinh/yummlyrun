package com.rizqi.yummlyrun.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rizqi on 16/03/2017.
 */

class Attribution {

    @SerializedName("html")
    @Expose
    public String html;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("text")
    @Expose
    public String text;
    @SerializedName("logo")
    @Expose
    public String logo;
}
