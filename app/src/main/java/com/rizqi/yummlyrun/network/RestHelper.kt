package com.rizqi.yummlyrun.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.rizqi.yummlyrun.BuildConfig


import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by rizqi on 15/03/2017.
 */

class RestHelper private constructor() {

    companion object {
        val instance by lazy { RestHelper() }
        const val CONNECTION_TIMEOUT: Long = 30 * 10 * 1000
        const val READ_TIMEOUT: Long = 30 * 10 * 1000
    }

    private fun buildRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(ApiConstant.BASE_URL)
                .client(buildHttpClient())
                .addConverterFactory(GsonConverterFactory.create(buildGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    private fun buildGson(): Gson {
        return GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create()
    }

    private fun buildHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                            else HttpLoggingInterceptor.Level.NONE

        val httpClient = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)

        return httpClient.build()
    }

    fun service(): RestService {
        return buildRetrofit().create(RestService::class.java)
    }
}
