package com.rizqi.yummlyrun.network;

import com.rizqi.yummlyrun.network.response.MatchResponse;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by rizqi on 15/03/2017.
 */

public interface RestService {

    @Headers({
            "X-Yummly-App-ID: ad950ea4",
            "X-Yummly-App-Key: 860795b786fdfb4713e10a8591c54177"
    })
    @GET(ApiConstant.BASE_LIST_FOOD)
    Observable<MatchResponse> getListFood(
            @Query("maxResult") int maxResult,
            @Query("start") int start
    );

    @Headers({
            "X-Yummly-App-ID: ad950ea4",
            "X-Yummly-App-Key: 860795b786fdfb4713e10a8591c54177"
    })
    @GET(ApiConstant.SEARCH_LIST_FOOD)
    Call<MatchResponse> getSearchFood(
            @Query("q") String query
    );


}
