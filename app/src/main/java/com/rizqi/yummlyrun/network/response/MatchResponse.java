package com.rizqi.yummlyrun.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rizqi on 16/03/2017.
 */

public class MatchResponse extends BaseResponse implements Parcelable{

    public MatchResponse(){

    }

    @SerializedName("matches")
    @Expose
    public List<Match> matches = null;

    public static class Match implements Parcelable{

        public Match(){

        }

        @SerializedName("imageUrlsBySize")
        @Expose
        public ImageUrlsBySize imageUrlsBySize;
        @SerializedName("sourceDisplayName")
        @Expose
        public String sourceDisplayName;
        @SerializedName("ingredients")
        @Expose
        public List<String> ingredients;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("smallImageUrls")
        @Expose
        public List<String> smallImageUrls;
        @SerializedName("recipeName")
        @Expose
        public String recipeName;
        @SerializedName("totalTimeInSeconds")
        @Expose
        public int totalTimeInSeconds;
        @SerializedName("attributes")
        @Expose
        public Attributes attributes;
        @SerializedName("flavors")
        @Expose
        public Flavors flavors;
        @SerializedName("rating")
        @Expose
        public int rating;

        protected Match(Parcel in) {
            sourceDisplayName = in.readString();
            ingredients = in.createStringArrayList();
            id = in.readString();
            smallImageUrls = in.createStringArrayList();
            recipeName = in.readString();
            totalTimeInSeconds = in.readInt();
            rating = in.readInt();
        }

        public static final Creator<Match> CREATOR = new Creator<Match>() {
            @Override
            public Match createFromParcel(Parcel in) {
                return new Match(in);
            }

            @Override
            public Match[] newArray(int size) {
                return new Match[size];
            }
        };

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Match match = (Match) o;

            if (totalTimeInSeconds != match.totalTimeInSeconds) return false;
            if (rating != match.rating) return false;
            if (!imageUrlsBySize.equals(match.imageUrlsBySize)) return false;
            if (!sourceDisplayName.equals(match.sourceDisplayName)) return false;
            if (!ingredients.equals(match.ingredients)) return false;
            if (!id.equals(match.id)) return false;
            if (!smallImageUrls.equals(match.smallImageUrls)) return false;
            if (!recipeName.equals(match.recipeName)) return false;
            if (!attributes.equals(match.attributes)) return false;
            return flavors.equals(match.flavors);

        }

        @Override
        public int hashCode() {
            int result = imageUrlsBySize.hashCode();
            result = 31 * result + sourceDisplayName.hashCode();
            result = 31 * result + ingredients.hashCode();
            result = 31 * result + id.hashCode();
            result = 31 * result + smallImageUrls.hashCode();
            result = 31 * result + recipeName.hashCode();
            result = 31 * result + totalTimeInSeconds;
            result = 31 * result + attributes.hashCode();
            result = 31 * result + flavors.hashCode();
            result = 31 * result + rating;
            return result;
        }

        @Override
        public String toString() {
            return "Match{" +
                    "imageUrlsBySize=" + imageUrlsBySize +
                    ", sourceDisplayName='" + sourceDisplayName + '\'' +
                    ", ingredients=" + ingredients +
                    ", id='" + id + '\'' +
                    ", smallImageUrls=" + smallImageUrls +
                    ", recipeName='" + recipeName + '\'' +
                    ", totalTimeInSeconds=" + totalTimeInSeconds +
                    ", attributes=" + attributes +
                    ", flavors=" + flavors +
                    ", rating=" + rating +
                    '}';
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(sourceDisplayName);
            dest.writeStringList(ingredients);
            dest.writeString(id);
            dest.writeStringList(smallImageUrls);
            dest.writeString(recipeName);
            dest.writeInt(totalTimeInSeconds);
            dest.writeInt(rating);
        }

        public static class ImageUrlsBySize {

            @SerializedName("90")
            @Expose
            public String _90;

        }

        public static class Attributes {

            @SerializedName("course")
            @Expose
            public List<String> course;

            @SerializedName("cuisine")
            @Expose
            public List<String> cuisine;

            @SerializedName("holiday")
            @Expose
            public List<String> holiday;
        }

        public static class Flavors {

            @SerializedName("piquant")
            @Expose
            public double piquant;
            @SerializedName("meaty")
            @Expose
            public double meaty;
            @SerializedName("bitter")
            @Expose
            public double bitter;
            @SerializedName("sweet")
            @Expose
            public double sweet;
            @SerializedName("sour")
            @Expose
            public double sour;
            @SerializedName("salty")
            @Expose
            public double salty;
        }

    }

    protected MatchResponse(Parcel in) {
        super(in);
        matches = in.createTypedArrayList(Match.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(matches);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MatchResponse> CREATOR = new Creator<MatchResponse>() {
        @Override
        public MatchResponse createFromParcel(Parcel in) {
            return new MatchResponse(in);
        }

        @Override
        public MatchResponse[] newArray(int size) {
            return new MatchResponse[size];
        }
    };
}
