package com.rizqi.yummlyrun.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.rizqi.yummlyrun.R;
import com.rizqi.yummlyrun.helper.preference.SaveSharedPreferences;
import com.rizqi.yummlyrun.helper.utils.StringUtil;
import com.rizqi.yummlyrun.network.RestHelper;
import com.rizqi.yummlyrun.network.RestService;
import com.rizqi.yummlyrun.network.response.MatchResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;

/**
 * Created by rizqi on 02/04/2017.
 */

public class ListSearchAdapter extends ArrayAdapter<String> implements Filterable {

    private final RestService mService = RestHelper.Companion.getInstance().service();
    private Context mContext;
    private int mResourceId;
    private List<MatchResponse.Match> resultList;
    private String keyword;


    public ListSearchAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
        mContext = context;
        mResourceId = resource;
        resultList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        MatchResponse.Match user = resultList.get(position);
        return getDisplayText(user);
    }

    public MatchResponse.Match getData(int position) {
        return resultList.get(position);
    }

    private String getDisplayText(MatchResponse.Match user) {
        StringBuffer tmp = new StringBuffer();
        if (user != null && keyword!=null) {
            if(user.recipeName!=null && user.recipeName.toLowerCase().contains(keyword.toLowerCase()))
                tmp.append(user.recipeName);
        }

        return tmp.toString();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(mResourceId, parent, false);
            }
            MatchResponse.Match user = resultList.get(position);
            String displayText = getDisplayText(user);

            TextView tv = (TextView) convertView.findViewById(R.id.tv_result_list);
            tv.setText(StringUtil.getTextWithBoldSpan(displayText, keyword));

            if (parent != null && (parent instanceof ListView)) {
                ListView lv = null;
                try {
                    lv = (ListView) parent;
                } catch (Exception e) {
                }
                if (lv != null) {
                    lv.setDivider(new ColorDrawable(Color.TRANSPARENT));
                    lv.setDividerHeight(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = new TextView(mContext);
        }

        final TextView item = (TextView) convertView;
        item.post(new Runnable() {
            @Override
            public void run() {
                item.setSingleLine(false);
                item.setMaxLines(2);
            }
        });

        if (parent != null && (parent instanceof ListView)) {
            ListView lv = null;
            try {
                lv = (ListView) parent;
            } catch (Exception e) {
            }
            if (lv != null) {
                lv.setDivider(new ColorDrawable(Color.TRANSPARENT));
                lv.setDividerHeight(0);
                lv.setMinimumHeight(100);
               // setListViewHeightBasedOnChildren(lv, 5);
            }
        }
        return item;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    keyword = constraint.toString();
                    // Retrieve the autocomplete results.
                    List<MatchResponse.Match> list = getSearchResult(keyword);
                    // Assign the data to the FilterResults
                    if (list != null) {
                        filterResults.values = list;
                        filterResults.count = list.size();
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<MatchResponse.Match>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    private List<MatchResponse.Match> getSearchResult(String keyword) {
        try {
            Call<MatchResponse> call = mService.getSearchFood(keyword);
            MatchResponse response = call.execute().body();
            return response.matches;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
