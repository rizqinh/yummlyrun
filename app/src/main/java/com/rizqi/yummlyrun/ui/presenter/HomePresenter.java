package com.rizqi.yummlyrun.ui.presenter;

/**
 * Created by rizqi on 15/03/2017.
 */

public interface HomePresenter {
    void getListFood(int treshold, int page);
}
