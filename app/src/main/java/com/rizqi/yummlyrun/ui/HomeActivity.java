package com.rizqi.yummlyrun.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.rizqi.yummlyrun.R;
import com.rizqi.yummlyrun.helper.preference.SaveSharedPreferences;
import com.rizqi.yummlyrun.ui.fragment.HomeFragment;

public class HomeActivity extends BaseActivity {

    public static final String TAG = HomeActivity.class.getName();
    private final SaveSharedPreferences mPreference = SaveSharedPreferences.getInstance();
    public static final String EX_USER = "ex_user";
    public static final String NEW_USER = "new_user";
    private Bundle mBundle = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindView();

       /* Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
           // doMySearch(query);

            Log.d("sasasasasasa", query.toString());
        }*/
    }

    @Override
    protected int getLayoutResID() {
        return R.layout.activity_home;
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            if (mBundle.getString(NEW_USER) != null) {
                Toast.makeText(this, "Hi "+mPreference.getNameInfo().toString()+", Welcome to Yummly", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Welcome Back " + mPreference.getNameInfo().toString(), Toast.LENGTH_SHORT).show();
            }
        }
        initView();

    }

    private void initView() {
        addFragment(R.id.fm_home_list,
                HomeFragment.newInstance(), HomeFragment.TAG);

    }

    @Override
    protected void onDestroy() {
        unbindView();
        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_home_search, menu);

        /*// Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();

// Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        searchView.setOnQueryTextListener(this);
        SearchView.SearchAutoComplete autoCompleteTextView = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);

        if (autoCompleteTextView != null) {
            autoCompleteTextView.setDropDownBackgroundResource(R.color.cardview_light_background);
        }*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
