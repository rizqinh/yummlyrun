package com.rizqi.yummlyrun.ui.widgets.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by rizqi on 17/03/2017.
 */

public class RobotoRegularTextView extends android.support.v7.widget.AppCompatTextView {
    public RobotoRegularTextView(Context context) {
        super(context);
    }

    public RobotoRegularTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RobotoRegularTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        textViewInit(attrs, defStyleAttr);
    }

    private void textViewInit(AttributeSet attrs, int defStyleAttr) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/Roboto-Regular.ttf");
        setTypeface(tf, defStyleAttr);
    }
}
