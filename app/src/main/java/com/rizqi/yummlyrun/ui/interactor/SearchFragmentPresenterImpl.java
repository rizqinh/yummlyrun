package com.rizqi.yummlyrun.ui.interactor;

import android.content.Context;

import com.rizqi.yummlyrun.network.RestHelper;
import com.rizqi.yummlyrun.network.RestService;
import com.rizqi.yummlyrun.network.response.MatchResponse;
import com.rizqi.yummlyrun.ui.fragment.SearchFragment;
import com.rizqi.yummlyrun.ui.presenter.SearchFragmentPresenter;
import com.rizqi.yummlyrun.ui.view.SearchFragmentView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rizqi on 03/04/2017.
 */

public class SearchFragmentPresenterImpl implements SearchFragmentPresenter {

    private final RestService mService = RestHelper.Companion.getInstance().service();
    private Context mContext;
    private SearchFragmentView mView;
    private Call<MatchResponse> callByKeyword;
    private Disposable mGetSearchFood = Disposables.empty();

    public SearchFragmentPresenterImpl(Context context, SearchFragmentView searchFragment) {
        this.mContext = context;
        this.mView = searchFragment;
    }

    @Override
    public void SearchKeyword(String searchkey) {
        callByKeyword = mService.getSearchFood(searchkey);
        callByKeyword.enqueue(new Callback<MatchResponse>() {
            @Override
            public void onResponse(Call<MatchResponse> call, Response<MatchResponse> response) {
                if (mView != null) {
                    if (response.isSuccessful() && response.body() != null) {
                        mView.onSearchResult(response.body().matches);
                    } else {
                        mView.onSearchFailed(new Throwable("Search failed"));
                    }
                }
            }

            @Override
            public void onFailure(Call<MatchResponse> call, Throwable t) {
                if (mView != null) {
                    mView.onSearchFailed(t);
                }
            }
        });
    }

}
