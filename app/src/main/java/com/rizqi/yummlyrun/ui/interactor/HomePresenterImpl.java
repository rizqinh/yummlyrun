package com.rizqi.yummlyrun.ui.interactor;

import android.content.Context;

import com.rizqi.yummlyrun.network.RestHelper;
import com.rizqi.yummlyrun.network.RestService;
import com.rizqi.yummlyrun.network.response.MatchResponse;
import com.rizqi.yummlyrun.ui.presenter.HomePresenter;
import com.rizqi.yummlyrun.ui.view.HomeView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by rizqi on 15/03/2017.
 */

public class HomePresenterImpl implements HomePresenter {

    private final RestService mService = RestHelper.Companion.getInstance().service();
    private Context mContext;
    private HomeView mView;
    private Disposable mGetListFood = Disposables.empty();

    public HomePresenterImpl(Context context, HomeView view) {
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getListFood(int page, int threshold) {
        mGetListFood = getListFoodData(page, threshold);
    }

    private Disposable getListFoodData(int page, int threshold) {
        return mService.getListFood(page, threshold)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<MatchResponse>() {
                    @Override
                    public void accept(MatchResponse match) throws Exception {
                        mView.onGetListFoodSuccess(match.matches);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        mView.onGetListFoodFailed(throwable);
                    }});
    }
}
