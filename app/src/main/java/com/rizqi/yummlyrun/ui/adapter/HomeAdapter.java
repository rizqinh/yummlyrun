package com.rizqi.yummlyrun.ui.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rizqi.yummlyrun.R;
import com.rizqi.yummlyrun.extra.listener.OnClickListener;
import com.rizqi.yummlyrun.helper.GlideHelper;
import com.rizqi.yummlyrun.network.response.MatchResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rizqi on 16/03/2017.
 */

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = HomeAdapter.class.getName();
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_FOOTER = 2;
    private Context mContext;
    private List<MatchResponse.Match> mItems;
    private OnClickListener onClickListener;

    private boolean isLoading;
    private int loadingViewPosition;

    public HomeAdapter(List<MatchResponse.Match> items) {
        mItems = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        if (viewType == TYPE_ITEM) {
            return getItemView(inflater, parent);
        }
        return getFooterView(inflater, parent);
    }

    private RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent) {
        View view = inflater.inflate(R.layout.layout_list_home, parent, false);
        ItemViewHolder holder = new ItemViewHolder(view);
        return holder;
    }

    private RecyclerView.ViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent) {
        View view = inflater.inflate(R.layout.footer_progressbar, parent, false);
        FooterViewHolder holder = new FooterViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            try {
                MatchResponse.Match mResponseData = mItems.get(position);
                itemViewHolder.tvTitleHome.setText(mResponseData.recipeName);
                String course = "";
                if (mResponseData.attributes.course != null) {
                    course = android.text.TextUtils.join(", ", mResponseData.attributes.course);
                }
                itemViewHolder.tvDescHome.setText(course);
                GlideHelper.loadImage(mContext, mResponseData.imageUrlsBySize._90, itemViewHolder.ivHomeList);
                itemViewHolder.rlHomeList.setOnClickListener(homeFoodListener);
                itemViewHolder.rlHomeList.setTag(R.id.rl_list_home, holder);
            } catch (IndexOutOfBoundsException ex) {
                Log.e(TAG, ex.getMessage());
            }
        } else if (holder instanceof FooterViewHolder) {
            //TODO
        }
    }

    View.OnClickListener homeFoodListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RecyclerView.ViewHolder vholder = (RecyclerView.ViewHolder) v.getTag(R.id.rl_list_home);
            int position = vholder.getAdapterPosition();

            if (onClickListener != null) {
                onClickListener.OnClick(v, position);
            }
        }
    };

    public void setOnClickListener(OnClickListener listener) {
        onClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading = isLoading;
        if (isLoading) {
            if (getItemCount() > loadingViewPosition && mItems.get(loadingViewPosition) == null) {
                mItems.remove(loadingViewPosition);
                notifyItemRemoved(loadingViewPosition);
            }

            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (getItemCount() > 0 && mItems.get(getItemCount() - 1) != null) {
                            mItems.add(null);
                            loadingViewPosition = getItemCount() - 1;
                            notifyItemInserted(loadingViewPosition);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "setIsLoading", e);
                    }
                }
            });
        } else {
            try {
                if (getItemCount() > loadingViewPosition && mItems.get(loadingViewPosition) == null) {
                    mItems.remove(loadingViewPosition);
                    notifyItemRemoved(loadingViewPosition);
                }
            } catch (Exception e) {
                Log.e(TAG, "setIsLoading", e);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoadingPosition(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isLoadingPosition(int position) {
        if (mItems != null && mItems.size() > 0) {
            return position == loadingViewPosition && mItems != null && mItems.get(position) == null;
        }
        return false;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title_home)
        TextView tvTitleHome;
        @BindView(R.id.tv_desc_home)
        TextView tvDescHome;
        @BindView(R.id.rl_list_home)
        RelativeLayout rlHomeList;
        @BindView(R.id.iv_food_home)
        ImageView ivHomeList;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        public FooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
