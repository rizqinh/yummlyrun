package com.rizqi.yummlyrun.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rizqi.yummlyrun.R;
import com.rizqi.yummlyrun.helper.GlideHelper;
import com.rizqi.yummlyrun.helper.utils.CommonUtility;
import com.rizqi.yummlyrun.helper.utils.TimeConvertUtil;
import com.rizqi.yummlyrun.network.response.MatchResponse;

import butterknife.BindView;

/**
 * Created by rizqi on 17/03/2017.
 */

public class HomeDetailActivity extends BaseActivity {

    public static MatchResponse.Match mMatchData;
    private int mutedcolor, mutedcolor2, mutedcolor4, mutedcolor3;

    @BindView(R.id.cl_home_detail)
    CoordinatorLayout clHomeDetail;
    @BindView(R.id.appbar_home_detail)
    AppBarLayout appbarHomeDetail;
    @BindView(R.id.iv_header_home_detail)
    ImageView ivHeaderHomeDetail;
    @BindView(R.id.toolbar_home_detail)
    Toolbar tbHomeDetail;
    @BindView(R.id.ct_home_detail)
    CollapsingToolbarLayout collapsingHomeDetail;
    @BindView(R.id.tv_time_cook)
    TextView tvTimeCook;
    @BindView(R.id.tv_ingredient_sum)
    TextView tvIngredients;
    @BindView(R.id.ratingBar_detail)
    RatingBar ratingBarDetail;
    @BindView(R.id.tv_recipename)
    TextView tvRecipeName;
    @BindView(R.id.tv_recipedesc)
    TextView tvRecipeDesc;
    @BindView(R.id.tv_title_toolbar)
    TextView tvToolbarTitle;
    @BindView(R.id.tv_desc_toolbar)
    TextView tvToolbarDesc;
    @BindView(R.id.list_info)
    ListView listInfo;
    @BindView(R.id.fab_detail_home)
    FloatingActionButton fabDetail;
    @BindView(R.id.rl_home_collapsing)
    RelativeLayout rvCollapsing;
    @BindView(R.id.linearLayout_categories)
    LinearLayout llCategories;
    @BindView(R.id.tv_rate)
    TextView tvRate;
   /* @BindView(R.id.rv_ingredients)
    RecyclerView rvInfo;
*/
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindView();
    }

    @Override
    protected void onDestroy() {
        mMatchData = null;
        unbindView();
        super.onDestroy();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //mBundle = getIntent().getExtras();
        if (mMatchData != null) {
            //setupPallete();
            initView();
        }
    }

    /*private void setupPallete() {
        Bitmap img = GlideHelper.loadImagetoBitmap(getApplicationContext(), mMatchData.imageUrlsBySize._90);
        if (img != null) {
            Palette.from(img).generate(new Palette.PaletteAsyncListener() {
                @Override
                public void onGenerated(Palette palette) {
                    Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();
                    mutedcolor = vibrantSwatch.getRgb();
                    mutedcolor2 = vibrantSwatch.getPopulation();
                    collapsingHomeDetail.setContentScrimColor(vibrantSwatch.getTitleTextColor());
                    collapsingHomeDetail.setStatusBarScrimColor(vibrantSwatch.getRgb());
                    tbHomeDetail.setBackgroundColor(vibrantSwatch.getRgb());
                }
            });
        }
    }*/

    private void initView() {
        setSupportActionBar(tbHomeDetail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingHomeDetail.setTitleEnabled(false);
        if (mMatchData.imageUrlsBySize._90 != null) {
            GlideHelper.loadWithPalette(this, mMatchData.imageUrlsBySize._90, ivHeaderHomeDetail, new Palette.PaletteAsyncListener() {
                @Override
                public void onGenerated(@NonNull Palette palette) {
                        Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();
                    if (vibrantSwatch!=null){
                        mutedcolor = vibrantSwatch.getRgb();
                        mutedcolor2 = vibrantSwatch.getPopulation();
                        mutedcolor3 = vibrantSwatch.getBodyTextColor();
                        mutedcolor4 = vibrantSwatch.getTitleTextColor();
                        collapsingHomeDetail.setContentScrimColor(vibrantSwatch.getTitleTextColor());
                        collapsingHomeDetail.setStatusBarScrimColor(vibrantSwatch.getRgb());
                    }else{
                        mutedcolor = getResources().getColor(R.color.transparentprimary50);
                        mutedcolor2 = getResources().getColor(R.color.primary);
                    }
                        llCategories.setBackgroundColor(mutedcolor);
                }
            });
        }

        Bitmap bmp = CommonUtility.textAsBitmap(mMatchData.rating+"", 14, getResources().getColor(android.R.color.white));

        fabDetail.setImageBitmap(bmp);

        tvRate.setText(mMatchData.rating+"");

        appbarHomeDetail.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isVisible = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                   // tbHomeDetail.setBackgroundResource(android.R.color.transparent);
                    tbHomeDetail.setBackgroundColor(mutedcolor2);
                    //tvToolbarTitle.setTextColor(mutedcolor3);
                    rvCollapsing.setVisibility(View.INVISIBLE);
                    tvToolbarTitle.setText(mMatchData.recipeName);
                    isVisible = true;
                } else if (isVisible) {
                    tvToolbarTitle.setText(mMatchData.recipeName);
                    rvCollapsing.setVisibility(View.VISIBLE);
                    tbHomeDetail.setBackgroundResource(R.drawable.background_color_gradient_2);
                    isVisible = false;
                }
            }
        });

        String time = TimeConvertUtil.ConvertRemain(mMatchData.totalTimeInSeconds);
        tvTimeCook.setText(time);
        String course ="";
        if (mMatchData.attributes.course!=null) {
            course = android.text.TextUtils.join(", ", mMatchData.attributes.course);
        }
        tvIngredients.setText(mMatchData.ingredients.size() + " " + getString(R.string.ingredients));
        ratingBarDetail.setRating(mMatchData.rating);
        tvRecipeName.setText(mMatchData.recipeName);
        tvRecipeDesc.setText(mMatchData.sourceDisplayName);
        tvToolbarDesc.setText(course);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                mMatchData.ingredients );

        listInfo.setAdapter(arrayAdapter);
    }

    @Override
    protected int getLayoutResID() {
        return R.layout.activity_home_detail;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;

    }
}
