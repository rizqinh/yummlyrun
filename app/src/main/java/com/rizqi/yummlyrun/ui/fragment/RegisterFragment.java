package com.rizqi.yummlyrun.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.rizqi.yummlyrun.R;
import com.rizqi.yummlyrun.extra.annotations.UserType;
import com.rizqi.yummlyrun.helper.preference.SaveSharedPreferences;
import com.rizqi.yummlyrun.ui.HomeActivity;

import butterknife.BindView;

/**
 * Created by rizqi on 27/03/2017.
 */

public class RegisterFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    public static final String TAG = RegisterFragment.class.getName();
    private final SaveSharedPreferences mPreference = SaveSharedPreferences.getInstance();

    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.et_name_reg)
    EditText etNameReg;
    @BindView(R.id.spinner_gender)
    Spinner spinnerGender;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnSubmit.setOnClickListener(this);
        setupSpinner();
    }

    private void setupSpinner() {

        ArrayAdapter dataAdapter = new ArrayAdapter(getActivity().getBaseContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.gender_items));
        // ArrayAdapter adapter = new ArrayAdapter<String>(getContext(), R.layout.item_spinner, listbank);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(dataAdapter);
        spinnerGender.setOnItemSelectedListener(this);
        dataAdapter.notifyDataSetChanged();
        spinnerGender.setPrompt(getResources().getString(R.string.gender));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int getLayoutResID() {
        return R.layout.fragment_register;
    }

    public static Fragment newInstance() {
        RegisterFragment fragment = new RegisterFragment();
        return fragment;
    }

    @Override
    public void onClick(View v) {
        if (etNameReg.getText().toString()!=""){
            mPreference.setNameInfo(etNameReg.getText().toString());
            mPreference.setGenderInfo(spinnerGender.getSelectedItem().toString());
            intentActicity();
        }else{
            Toast toast = Toast.makeText(getContext(),"Please Insert Your Data", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 0);
            toast.show();
        }

    }

    private void intentActicity() {
        Intent intent = new Intent(getActivity().getBaseContext(), HomeActivity.class);
        intent.putExtra(HomeActivity.NEW_USER, UserType.NEW_USER);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        spinnerGender.setSelection(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        spinnerGender.setPrompt(getResources().getString(R.string.gender));
    }
}
