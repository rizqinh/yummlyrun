package com.rizqi.yummlyrun.ui.view;

import com.rizqi.yummlyrun.network.response.MatchResponse;

import java.util.List;

/**
 * Created by rizqi on 03/04/2017.
 */

public interface SearchFragmentView {

    void onSearchResult(List<MatchResponse.Match> matches);

    void onSearchFailed(Throwable throwable);
}
