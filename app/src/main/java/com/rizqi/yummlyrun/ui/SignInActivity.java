package com.rizqi.yummlyrun.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;

import com.rizqi.yummlyrun.R;
import com.rizqi.yummlyrun.extra.annotations.UserType;
import com.rizqi.yummlyrun.helper.preference.SaveSharedPreferences;
import com.rizqi.yummlyrun.ui.fragment.HomeFragment;
import com.rizqi.yummlyrun.ui.fragment.SignInFragment;

/**
 * Created by rizqi on 17/03/2017.
 */

public class SignInActivity extends BaseActivity {

    private final SaveSharedPreferences mPreference = SaveSharedPreferences.getInstance();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        bindView();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        if (mPreference.getNameInfo()=="") {
            addFragment(R.id.fm_signin,
                    SignInFragment.newInstance(), SignInFragment.TAG);
        }else{
            Intent intent = new Intent(this, HomeActivity.class);
            intent.putExtra(HomeActivity.EX_USER, UserType.EX_USER);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        unbindView();
        super.onDestroy();
    }

    @Override
    protected int getLayoutResID() {
        return R.layout.activity_signin;
    }
}
