package com.rizqi.yummlyrun.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.rizqi.yummlyrun.R;
import com.rizqi.yummlyrun.ui.SignInActivity;

import butterknife.BindView;

/**
 * Created by rizqi on 26/03/2017.
 */

public class SignInFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = SignInFragment.class.getName();

    @BindView(R.id.btn_signin)
    Button btnSignin;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnSignin.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int getLayoutResID() {
        return R.layout.fragment_signin;
    }

    public static Fragment newInstance() {
        SignInFragment fragment = new SignInFragment();
        return fragment;
    }

    @Override
    public void onClick(View v) {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fm_signin, RegisterFragment.newInstance(), RegisterFragment.TAG);
        ft.addToBackStack(null);
        ft.commit();
    }
}
