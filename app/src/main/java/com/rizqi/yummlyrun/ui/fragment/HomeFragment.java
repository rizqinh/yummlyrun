package com.rizqi.yummlyrun.ui.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.rizqi.yummlyrun.R;
import com.rizqi.yummlyrun.extra.listener.OnClickListener;
import com.rizqi.yummlyrun.extra.listener.PaginationScrollListener;
import com.rizqi.yummlyrun.helper.utils.CommonUtility;
import com.rizqi.yummlyrun.helper.utils.ConnectionUtils;
import com.rizqi.yummlyrun.network.response.MatchResponse;
import com.rizqi.yummlyrun.ui.HomeDetailActivity;
import com.rizqi.yummlyrun.ui.adapter.HomeAdapter;
import com.rizqi.yummlyrun.ui.interactor.HomePresenterImpl;
import com.rizqi.yummlyrun.ui.presenter.HomePresenter;
import com.rizqi.yummlyrun.ui.view.HomeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by rizqi on 20/03/2017.
 */

public class HomeFragment extends BaseFragment implements HomeView, OnClickListener, SearchView.OnQueryTextListener {

    public static final String TAG = HomeFragment.class.getName();
    private HomePresenter mPresenter;
    private List<MatchResponse.Match> mItems = new ArrayList<>();
    private HomeAdapter mAdapter;
    private int totalItemsCount;
    private boolean isPullToRefresh = true;
    private View mView;

    @BindView(R.id.rv_home)
    RecyclerView rvHome;
    @BindView(R.id.sr_home_list_food)
    SwipeRefreshLayout srRefresh;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRefresh();
        setupSwipeRefresh();
        initView();
        GetListFood();
    }

    private void setupSwipeRefresh() {
        srRefresh.setEnabled(true);
        srRefresh.post(new Runnable() {
            @Override
            public void run() {
                srRefresh.setRefreshing(true);
            }
        });
    }


    private void initRefresh() {
        srRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isPullToRefresh = true;
                refreshData();
                initView();
                GetListFood();
            }
        });
    }

    private void refreshData() {
        if (mItems != null) {
            mItems.clear();
            mAdapter.notifyDataSetChanged();
        }
    }

    private void initInstance() {
        mPresenter = new HomePresenterImpl(getContext(), this);
    }

    private void initView() {
        mAdapter = new HomeAdapter(mItems);
        rvHome.setHasFixedSize(true);
        rvHome.setItemAnimator(new DefaultItemAnimator());
        rvHome.setAdapter(mAdapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (mAdapter.getItemViewType(position)) {
                    case HomeAdapter.TYPE_ITEM:
                        return 1;
                    case HomeAdapter.TYPE_FOOTER:
                        return 2; //number of columns of the grid
                    default:
                        return -1;
                }
            }
        });
        mRecyclerViewOnScrollListener.setThresholdCount(CommonUtility.threshold());
        mRecyclerViewOnScrollListener.setLayoutManager(layoutManager);
        rvHome.setLayoutManager(layoutManager);
        mAdapter.setOnClickListener(this);
        rvHome.addOnScrollListener(mRecyclerViewOnScrollListener);
    }

    private final PaginationScrollListener mRecyclerViewOnScrollListener = new PaginationScrollListener() {
        @Override
        public void loadMore(int page) {
            if (!ConnectionUtils.isConnected(getContext())) {
                Snackbar.make(mView, "Check Your Connection", Snackbar.LENGTH_SHORT).show();
                return;
            }

            isPullToRefresh = false;
            mAdapter.setIsLoading(true);
            mRecyclerViewOnScrollListener.setIsLoading(true);
            mPresenter.getListFood(CommonUtility.threshold(), page);
        }

        @Override
        public void onPageUp() {

        }

        @Override
        public void onPageDown() {

        }
    };

    private void GetListFood() {
        refreshData();
        totalItemsCount = 0;
        mRecyclerViewOnScrollListener.setIsLoading(true);
        mPresenter.getListFood(CommonUtility.threshold(), totalItemsCount);
    }

    @Override
    public void onGetListFoodSuccess(List<MatchResponse.Match> matches) {
        if (matches != null) {
            mAdapter.setIsLoading(false);
            srRefresh.setRefreshing(false);
            appendUI(matches);
            mRecyclerViewOnScrollListener.setIsLastPage(false);
            mRecyclerViewOnScrollListener.setIsLoading(false);
            totalItemsCount += matches.size();
            mRecyclerViewOnScrollListener.setPagination(totalItemsCount);
        }
    }

    private void appendUI(List<MatchResponse.Match> matches) {
        mItems.addAll(matches);
        if (isPullToRefresh) {
            mAdapter.notifyDataSetChanged();
        } else {
            mAdapter.notifyItemChanged(mItems.size() + 1, matches.size());
        }
    }

    @Override
    public void onGetListFoodFailed(Throwable throwable) {
        Log.d("onGetListFoodFailed", throwable.getMessage());
        Snackbar.make(mView, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
        srRefresh.setRefreshing(false);
        mAdapter.setIsLoading(false);
        mRecyclerViewOnScrollListener.setIsLastPage(false);
        mRecyclerViewOnScrollListener.setIsLoading(false);
    }

    @Override
    public void OnClick(View view, int position) {
        Intent profileIntent = new Intent(getActivity(), HomeDetailActivity.class);
        HomeDetailActivity.mMatchData = mItems.get(position);
        startActivity(profileIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int getLayoutResID() {
        return R.layout.fragment_home;
    }

    public static Fragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_home_search, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getContext().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();

// Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        searchView.setOnQueryTextListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
