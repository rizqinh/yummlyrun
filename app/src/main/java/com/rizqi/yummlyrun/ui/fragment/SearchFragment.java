package com.rizqi.yummlyrun.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rizqi.yummlyrun.R;
import com.rizqi.yummlyrun.network.response.MatchResponse;
import com.rizqi.yummlyrun.ui.HomeDetailActivity;
import com.rizqi.yummlyrun.ui.adapter.HomeAdapter;
import com.rizqi.yummlyrun.ui.adapter.ListSearchAdapter;
import com.rizqi.yummlyrun.ui.interactor.SearchFragmentPresenterImpl;
import com.rizqi.yummlyrun.ui.presenter.SearchFragmentPresenter;
import com.rizqi.yummlyrun.ui.view.SearchFragmentView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by rizqi on 26/03/2017.
 */

public class SearchFragment extends BaseFragment implements SearchFragmentView {

    private List<MatchResponse.Match> mResult;
    private HomeAdapter mSearchAdapter;
    private ListSearchAdapter mAtvAdapter;
    private SearchFragmentPresenter mPresenter;

    @BindView(R.id.ll_search_header)
    LinearLayout llSearchHeader;
    @BindView(R.id.ll_search_result)
    LinearLayout llSearchResult;
    @BindView(R.id.atv_search)
    AutoCompleteTextView atvSearch;


    public static final String TAG = SearchFragment.class.getName();
    private static final String PARAM_SEARCH = "search_param";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInstance();
    }

    private void initInstance() {
        mResult = new ArrayList<>();
        mSearchAdapter = new HomeAdapter(mResult);
        mPresenter = new SearchFragmentPresenterImpl(getContext(), this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        mAtvAdapter = new ListSearchAdapter(getContext(), R.layout.fragment_list_search);
        atvSearch.setAdapter(mAtvAdapter);
        atvSearch.setOnItemClickListener(atvItemClickListener);
        atvSearch.setOnEditorActionListener(atvEditorListener);
        atvSearch.setOnTouchListener(atvOnTouchListener);
        atvSearch.addTextChangedListener(atvTextWatcherListener);
    }

    private AdapterView.OnItemClickListener atvItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            MatchResponse.Match food = mAtvAdapter.getData(position);
            showFoodDetail(food);
        }
    };

    private void showFoodDetail(MatchResponse.Match food) {
        Intent profileIntent = new Intent(getActivity(), HomeDetailActivity.class);
        HomeDetailActivity.mMatchData = food;
        startActivity(profileIntent);
    }

    private TextView.OnEditorActionListener atvEditorListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH || (event != null && event.getAction() == KeyEvent.ACTION_UP && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                if (atvSearch.getText().length() > 0) {
                    atvSearch.dismissDropDown();
                    InputMethodManager in = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    SearchByKeyword(atvSearch.getText().toString());
                }
                return true;
            }

            return event != null && event.getAction() == KeyEvent.ACTION_DOWN;
        }
    };

    private void SearchByKeyword(String searchkey) {
        clearSearchResult();
        showResultLayout();
        mPresenter.SearchKeyword(searchkey);
    }

    private void showResultLayout() {
        llSearchResult.setVisibility(View.VISIBLE);
    }

    private void clearSearchResult() {
        mResult.clear();
        mSearchAdapter.notifyDataSetChanged();
    }

    private View.OnTouchListener atvOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return false;
        }
    };

    private TextWatcher atvTextWatcherListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int getLayoutResID() {
        return R.layout.fragment_search;
    }

    public static Fragment newInstance(String query) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_SEARCH, query);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSearchResult(List<MatchResponse.Match> matches) {
        if (matches != null) {
            mResult.addAll(matches);
        }
    }

    @Override
    public void onSearchFailed(Throwable throwable) {

    }
}
