package com.rizqi.yummlyrun.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.rizqi.yummlyrun.R;
import com.rizqi.yummlyrun.ui.fragment.SearchFragment;

/**
 * Created by rizqi on 30/03/2017.
 */

public class SearchActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindView();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        addFragment(R.id.fm_search_list,
                SearchFragment.newInstance(""), SearchFragment.TAG);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        unbindView();
        super.onDestroy();
    }


    @Override
    protected int getLayoutResID() {
        return R.layout.activiy_search;
    }
}
