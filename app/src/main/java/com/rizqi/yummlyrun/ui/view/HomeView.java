package com.rizqi.yummlyrun.ui.view;

import com.rizqi.yummlyrun.network.response.MatchResponse;

import java.util.List;

/**
 * Created by rizqi on 15/03/2017.
 */

public interface HomeView {
    void onGetListFoodSuccess(List<MatchResponse.Match> matches);

    void onGetListFoodFailed(Throwable throwable);
}
