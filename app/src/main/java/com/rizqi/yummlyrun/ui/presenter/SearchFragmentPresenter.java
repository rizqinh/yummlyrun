package com.rizqi.yummlyrun.ui.presenter;

/**
 * Created by rizqi on 03/04/2017.
 */

public interface SearchFragmentPresenter {
    void SearchKeyword(String searchkey);
}
