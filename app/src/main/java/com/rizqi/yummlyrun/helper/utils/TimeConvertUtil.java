package com.rizqi.yummlyrun.helper.utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by rizqi on 27/03/2017.
 */

public class TimeConvertUtil {

    public static String ConvertRemain(int millis) {
        if(millis < 0) {
            throw new IllegalArgumentException("Less than 0");
        }

        int hours = millis / 3600;
        int minutes = (millis % 3600) / 60;
        int seconds = (millis % 3600) % 60;

        String minutes_total = minutes+" Minutes";

        return minutes_total;
    }

}
