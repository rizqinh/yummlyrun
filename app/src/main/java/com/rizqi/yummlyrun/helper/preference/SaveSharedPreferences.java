package com.rizqi.yummlyrun.helper.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.rizqi.yummlyrun.YummlyAppDroid;

/**
 * Created by rizqi on 30/03/2017.
 */

public class SaveSharedPreferences {

    public static final String INFO_NAME = "info_name";
    public static final String INFO_GENDER = "info_gender";
    public static final String PREF_NAME = "perf_name";

    private Context mContext;
    private ObscuredSharedPreferences mObscuredSharedPreferences;

    private static final Object LOCK = new Object();
    private static SaveSharedPreferences sInstance;

    public static SaveSharedPreferences getInstance(){
        if (sInstance == null) {
            sInstance = new SaveSharedPreferences();
        }
        return sInstance;
    }

    public SaveSharedPreferences() {
        this.mContext = YummlyAppDroid.getContext();
        SharedPreferences preferences = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        mObscuredSharedPreferences = new ObscuredSharedPreferences(mContext, preferences);
    }

    public void set(String key, String value) {
        mObscuredSharedPreferences.edit().putString(key, value).apply();
    }

    public void set(String key, int value) {
        mObscuredSharedPreferences.edit().putInt(key, value).apply();
    }

    public void set(String key, boolean value) {
        mObscuredSharedPreferences.edit().putBoolean(key, value).apply();
    }

    public void clear(String key) {
        mObscuredSharedPreferences.edit().remove(key).apply();
    }

    public void clearAll() {
        mObscuredSharedPreferences.edit().clear().apply();
    }

    public String getNameInfo() {
        return mObscuredSharedPreferences.getString(INFO_NAME, "");
    }

    public void setNameInfo(String value){
        mObscuredSharedPreferences.edit().putString(INFO_NAME, value).apply();
    }

    public String getGenderInfo() {
        return mObscuredSharedPreferences.getString(INFO_GENDER, "");
    }

    public void setGenderInfo(String value){
        mObscuredSharedPreferences.edit().putString(INFO_GENDER, value).apply();
    }
}
