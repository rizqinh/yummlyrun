package com.rizqi.yummlyrun.helper.utils;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;

/**
 * Created by rizqi on 03/04/2017.
 */

public class StringUtil {

    public static SpannableStringBuilder getTextWithBoldSpan(String text, String spanText) {
        return getTextWithSpan(text, spanText, new StyleSpan(Typeface.BOLD), true);
    }

    public static SpannableStringBuilder getTextWithSpan(String text, String spanText, StyleSpan style, boolean fullWord) {
        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        if (text != null && !text.isEmpty()) {
            int spanLen = spanText.length();
            int start = text.toLowerCase().indexOf(spanText.toLowerCase());
            if (start >= 0) {
                int end = start + spanLen;
                if (fullWord) {
                    end = text.length();
                    int spacePos = text.toLowerCase().indexOf(" ", start + spanLen);
                    if (spacePos > start)
                        end = spacePos;
                }
                sb.setSpan(style, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            }
        }
        return sb;
    }
}
