package com.rizqi.yummlyrun.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.graphics.Palette;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

/**
 * Created by rizqi on 16/03/2017.
 */

public class GlideHelper {

    public static void loadImage(Context context, final String url, final ImageView resId) {

        RequestOptions ro = new RequestOptions()
                .dontAnimate();

        Glide.with(context)
                .load(url)
                .apply(ro)
                .into(resId);
    }

    public static Bitmap loadImagetoBitmap(Context context, final String url) {
        try {
            return Glide.with(context)
                    .asBitmap()
                     .load(url)
                     .into(-1, -1)
                    .get();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


    public static void loadImagewithPlaceHolder(Context context, final String url, final ImageView resId, final int placeholder) {
        RequestOptions ro = new RequestOptions()
                .centerCrop()
                .placeholder(placeholder)
                .dontAnimate();

        Glide.with(context)
                .load(url)
                .apply(ro)
                .into(resId);
    }

    public static void loadWithPalette(Context context, @NonNull final String url, @NonNull final ImageView resId, final Palette.PaletteAsyncListener palletelistener){
        RequestOptions req = new RequestOptions()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);

        Glide.with(context)
                .asBitmap()
                .load(url)
                .apply(req)
                /*.listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        //resId.setImageBitmap(resource);
                        Palette.from(resource).generate(palletelistener);
                        return false;
                    }

                    private void onPalette(Palette generate) {
                        if (null != generate) {
                            ViewGroup parent = (ViewGroup) resId.getParent().getParent();
                            Palette.Swatch vibrantSwatch = generate.getVibrantSwatch();
                            parent.setBackgroundColor(vibrantSwatch.getPopulation());
                            //parent.setBackgroundColor(generate.getMutedColor(ContextCompat.getColor(YummlyAppDroid.getContext(), R.color.primary)));
                        }
                    }
                })*/
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                            //resId.setImageBitmap(resource);
                            Palette.from(resource).generate(palletelistener);
                            return false;
                        }

                        private void onPallete(Palette generate){
                            if (null != generate) {
                                ViewGroup parent = (ViewGroup) resId.getParent().getParent();
                                Palette.Swatch vibrantSwatch = generate.getVibrantSwatch();
                                parent.setBackgroundColor(vibrantSwatch.getPopulation());
                                //parent.setBackgroundColor(generate.getMutedColor(ContextCompat.getColor(YummlyAppDroid.getContext(), R.color.primary)));
                            }
                        }
                    })
                .into(resId);
    }

    public static void GlideBitmap(Context context, final String url, final ImageView resId){
        RequestOptions ro = new RequestOptions()
                .fitCenter()
                .override(1600, 1600)
                .dontAnimate();

        Glide.with(context)
                .asBitmap()
                .load(url)
                .apply(ro)
                .into(resId);
    }


}
