package com.rizqi.yummlyrun.extra.listener;

import android.view.View;

/**
 * Created by rizqi on 16/03/2017.
 */

public interface OnClickListener {

    void OnClick(View view, int position);
}
