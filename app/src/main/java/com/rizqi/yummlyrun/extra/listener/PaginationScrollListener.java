package com.rizqi.yummlyrun.extra.listener;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.rizqi.yummlyrun.helper.utils.CommonUtility;

/**
 * Created by rizqi on 24/03/2017.
 */

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {
    private static final String TAG = PaginationScrollListener.class.getSimpleName();

    private GridLayoutManager mGridLayoutManager;
    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private LinearLayoutManager mLinearLayoutManager;

    private boolean isLoading = false;
    private boolean isLastPage = false;

    public int mThreshold = CommonUtility.threshold();

    public void setThresholdCount(int threshold) {
        mThreshold = threshold;
    }

    public void setLayoutManager(GridLayoutManager manager) {
        mGridLayoutManager = manager;
    }

    public void setLayoutManager(StaggeredGridLayoutManager manager) {
        mStaggeredGridLayoutManager = manager;
    }

    public void setLayoutManager(LinearLayoutManager manager) {
        mLinearLayoutManager = manager;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public void setIsLastPage(boolean isLastPage) {
        this.isLastPage = isLastPage;
    }

    public void setPagination(int pagination) {
        if (pagination > mThreshold)
            this.mThreshold = pagination;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        this.y = dy;
        int visibleItemCount = -1;
        int totalItemCount = -1;
        int firstVisibleItemPosition = -1;

        if (mGridLayoutManager != null) {
            visibleItemCount = mGridLayoutManager.getChildCount();
            totalItemCount = mGridLayoutManager.getItemCount();
            firstVisibleItemPosition = mGridLayoutManager.findFirstVisibleItemPosition();
        } else if (mLinearLayoutManager != null) {
            visibleItemCount = mLinearLayoutManager.getChildCount();
            totalItemCount = mLinearLayoutManager.getItemCount();
            firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();
        }

        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= mThreshold) {
                isLoading = true;
                isLastPage = true;
                loadMore(mThreshold);
            }
        }
    }

    private int y;

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if (RecyclerView.SCROLL_STATE_IDLE == newState) {
            if (y <= 0) {
                onPageUp();
            } else {
                onPageDown();
                y = 0;
            }
        }
    }

    public abstract void loadMore(int page);

    public abstract void onPageUp();

    public abstract void onPageDown();
}
