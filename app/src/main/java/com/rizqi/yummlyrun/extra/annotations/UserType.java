package com.rizqi.yummlyrun.extra.annotations;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.rizqi.yummlyrun.extra.annotations.UserType.EX_USER;
import static com.rizqi.yummlyrun.extra.annotations.UserType.NEW_USER;

/**
 * Created by rizqi on 31/03/2017.
 */


@StringDef(value={
        NEW_USER,
        EX_USER
})
@Retention(RetentionPolicy.SOURCE)

public @interface UserType {
    String NEW_USER = "new_user";
    String EX_USER = "ex_user";
}
